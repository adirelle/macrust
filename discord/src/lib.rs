mod config;

use std::sync::Arc;

use anyhow::Result;
pub use config::*;

use poise::serenity_prelude::{self as serenity, Client, CreateMessage, Message, MessageBuilder};
use tracing::Instrument;

pub struct Bot {
    client: Client,
}

#[derive(Debug, Default)]
pub struct Data {}

impl Bot {
    pub async fn new(config: Arc<Config>) -> Result<Self> {
        let intents = serenity::GatewayIntents::non_privileged();
        let token = config.token.clone();

        let framework = poise::Framework::builder()
            .options(poise::FrameworkOptions::<Data, anyhow::Error> {
                commands: vec![],
                on_error: |error| {
                    Box::pin(async move {
                        tracing::info!(?error, "discord error");
                    })
                },
                ..Default::default()
            })
            .setup(|ctx, _ready, framework| {
                Box::pin(async move {
                    poise::builtins::register_globally(ctx, &framework.options().commands).await?;
                    let message = CreateMessage::new().content("hello !");
                    ctx.http
                        .send_message(config.channel_id, vec![], &message)
                        .await?;
                    Ok(Data {})
                })
            })
            .build();

        let client = serenity::ClientBuilder::new(token, intents)
            .framework(framework)
            .await?;

        Ok(Self { client })
    }

    pub async fn run(mut self) -> Result<()> {
        tracing::info!("starting bot");
        self.client.start().await?;

        Ok(())
    }
}
