use anyhow::*;
use poise::serenity_prelude::{ChannelId, GuildId};
use serde::Deserialize;

#[derive(Debug, Deserialize)]
pub struct ConfigBuilder {
    token: String,
    guild_id: GuildId,
    channel_id: ChannelId,
}

impl ConfigBuilder {
    pub fn token(mut self, token: String) -> Self {
        self.token = token;
        self
    }

    pub fn build(self) -> Result<Config> {
        let Self {
            token,
            guild_id,
            channel_id,
        } = self;
        Ok(Config {
            token,
            guild_id,
            channel_id,
        })
    }
}

#[derive(Debug)]
pub struct Config {
    pub(crate) token: String,
    pub(crate) guild_id: GuildId,
    pub(crate) channel_id: ChannelId,
}
