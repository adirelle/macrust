use std::io::stderr;

use serde::Deserialize;
use tracing::level_filters::LevelFilter;
use tracing::subscriber::set_global_default;
use tracing::Level;
use tracing_appender::non_blocking::WorkerGuard;
use tracing_subscriber::filter::filter_fn;
use tracing_subscriber::layer::SubscriberExt;
use tracing_subscriber::{Layer, Registry};

#[derive(Debug, Deserialize)]
pub struct Logging {
    #[serde(with = "LevelDef")]
    level: Level,
}

impl Default for Logging {
    fn default() -> Self {
        Self {
            level: Level::DEBUG,
        }
    }
}

impl Logging {
    pub fn init(self) -> WorkerGuard {
        let (non_blocking, guard) = tracing_appender::non_blocking(stderr());

        let subscriber = Registry::default().with(
            tracing_subscriber::fmt::layer()
                .with_writer(non_blocking)
                .with_target(false)
                .compact()
                .with_filter(LevelFilter::from_level(self.level))
                .with_filter(filter_fn(|metadata| {
                    metadata.target().starts_with("macrust")
                })),
        );

        set_global_default(subscriber).expect("Unable to set global subscriber");

        guard
    }
}

#[derive(Debug, Deserialize)]
#[serde(remote = "Level")]
#[allow(clippy::upper_case_acronyms)]
enum LevelDef {
    TRACE,
    DEBUG,
    INFO,
    WARN,
    ERROR,
}
