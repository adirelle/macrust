mod config;
mod logging;

use std::sync::Arc;

use anyhow::Result;
use macrust_discord::Bot;
use macrust_minecraft::{Controller, Event, Message};
use tokio::signal::ctrl_c;
use tokio::signal::unix::{signal, SignalKind};
use tokio::task::JoinSet;
use tokio_util::sync::CancellationToken;
use tracing::{instrument, Instrument};

use crate::config::Config;

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let Config {
        logging,
        minecraft,
        discord,
    } = config::ConfigBuilder::read()?;
    let _guard = logging.init();

    let ctoken = CancellationToken::new();
    let mut tasks = JoinSet::<Result<()>>::new();

    tasks.spawn(run_minecraft(minecraft, ctoken.clone()));
    tasks.spawn(run_bot(discord, ctoken.clone()));

    signaled().await;

    ctoken.cancel();

    {
        let kill_it = signaled();
        tokio::pin!(kill_it);
        while !tasks.is_empty() {
            tracing::debug!(remaining=?tasks.len(), "waiting for services to end");
            tokio::select! {
                _ = tasks.join_next() => {
                    // NOOP
                }
                _ = &mut kill_it  => {
                    tracing::debug!(remaining=?tasks.len(), "aborting remaining services");
                    tasks.shutdown().await;
                    break;
                }
            }
        }
    }

    Ok(())
}

#[instrument(name = "minecraft", skip_all)]
async fn run_minecraft(config: macrust_minecraft::Config, ctoken: CancellationToken) -> Result<()> {
    let mut ctl = Controller::new(Arc::new(config));
    let mut events = ctl.events();

    tokio::spawn(
        async move {
            while let Ok(event) = events.recv().await {
                match event {
                    Event::Message {
                        message: e @ Message::Exception { .. },
                    } => {
                        tracing::warn!(message=?e.message(), "exception")
                    }
                    Event::Message { message } => {
                        tracing::trace!(message=?message.message(), "message")
                    }
                    _ => tracing::info!(?event, "event"),
                }
            }
        }
        .instrument(tracing::info_span!("events")),
    );

    tracing::info!("starting the server");
    ctl.start()?;

    ctoken.cancelled().await;

    tracing::info!("stopping the server");
    ctl.terminate().await
}

#[instrument(name = "discord", skip_all)]
async fn run_bot(config: macrust_discord::Config, _ctoken: CancellationToken) -> Result<()> {
    tracing::info!("connecting to discord");
    let bot = Bot::new(Arc::new(config)).await?;
    tracing::info!("bot started");
    bot.run().await
}

async fn signaled() {
    let mut sigterm = signal(SignalKind::terminate()).unwrap();
    let signal = tokio::select! {
        _ = ctrl_c() => "SIGINT",
        _ = sigterm.recv() => "SIGTERM"
    };
    tracing::info!(?signal, "got signal");
}
