use anyhow::Result;
use config::Environment;
use serde::Deserialize;

use crate::logging::Logging;

#[derive(Debug, Deserialize)]
pub struct ConfigBuilder {
    minecraft: macrust_minecraft::ConfigBuilder,
    logging: Logging,
    discord: macrust_discord::ConfigBuilder,
}

impl ConfigBuilder {
    pub fn read() -> Result<Config> {
        use config::{File, FileFormat};
        config::Config::builder()
            .add_source(File::new("config.toml", FileFormat::Toml).required(false))
            .add_source(Environment::with_prefix("MACRUST"))
            .build()?
            .try_deserialize::<'_, ConfigBuilder>()?
            .build()
    }

    pub fn build(self) -> Result<Config> {
        let Self {
            minecraft,
            logging,
            discord,
        } = self;
        Ok(Config {
            minecraft: minecraft.build()?,
            logging,
            discord: discord.build()?,
        })
    }
}

pub struct Config {
    pub(crate) minecraft: macrust_minecraft::Config,
    pub(crate) logging: Logging,
    pub(crate) discord: macrust_discord::Config,
}
