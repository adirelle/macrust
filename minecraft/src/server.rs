use std::process::{ExitStatus, Stdio};
use std::sync::Arc;

use anyhow::Result;
use futures::{SinkExt, StreamExt};
use tokio::io::{AsyncRead, AsyncWrite};
use tokio::process::Child;
use tokio::sync::{broadcast, mpsc};
use tokio::task::JoinSet;
use tokio_util::codec::{FramedRead, FramedWrite};
use tracing::{field, Instrument, Span};

use super::{config::*, event::*, input::*, output::Decoder};

// ======================================== Server ========================================

#[derive(Debug)]
pub struct Server;

impl Server {
    pub async fn run(
        config: Arc<Config>,
        cmds: mpsc::Receiver<Command>,
        events: broadcast::Sender<Event>,
    ) {
        let result = Self::run_inner(config, cmds, events.clone()).await;
        _ = events.send(result.into());
    }

    #[tracing::instrument(level = "DEBUG", name="server", fields(pid = field::Empty) skip_all, ret)]
    async fn run_inner(
        config: Arc<Config>,
        cmds: mpsc::Receiver<Command>,
        events: broadcast::Sender<Event>,
    ) -> Result<ExitStatus> {
        let mut child = Self::spawn(config)?;
        let pid = child.id().unwrap();
        Span::current().record("pid", pid);
        events.send(Event::server(ServerEvent::Starting { pid }))?;

        let mut tasks = JoinSet::<Result<()>>::new();

        tasks.spawn(
            Self::send_commands(child.stdin.take().unwrap(), cmds)
                .instrument(tracing::info_span!("stdin")),
        );

        tasks.spawn(
            Self::process_output(child.stdout.take().unwrap(), events.clone())
                .instrument(tracing::info_span!("stdout")),
        );

        tasks.spawn(
            Self::process_output(child.stderr.take().unwrap(), events)
                .instrument(tracing::info_span!("stderr")),
        );

        let result = child.wait().await;
        tasks.shutdown().await;

        result.map_err(Into::into)
    }

    fn spawn(config: Arc<Config>) -> Result<Child> {
        config
            .command()
            .stdin(Stdio::piped())
            .stdout(Stdio::piped())
            .stderr(Stdio::piped())
            .kill_on_drop(true)
            .spawn()
            .map_err(Into::into)
    }

    async fn process_output<T: AsyncRead + Unpin>(
        src: T,
        events: broadcast::Sender<Event>,
    ) -> Result<()> {
        tokio::pin! {
            let stream = FramedRead::new(src, Decoder::default()).map(|item| item.and_then(Event::try_from));
        };
        while let Some(event) = stream.next().await.transpose()? {
            events.send(event)?;
        }
        tracing::debug!("event stream closed");
        Ok(())
    }

    async fn send_commands<T: AsyncWrite>(dst: T, mut cmds: mpsc::Receiver<Command>) -> Result<()> {
        tokio::pin! {
            let sink = FramedWrite::new(dst, Encoder);
        };
        while let Some(cmd) = cmds.recv().await {
            tracing::debug!(?cmd, "sending command");
            sink.send(cmd).await?;
        }
        tracing::debug!("command stream closed");
        Ok(())
    }
}
