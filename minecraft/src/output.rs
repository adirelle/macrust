mod message;

use std::borrow::Cow;

use anyhow::Result;
use bytes::{Buf, Bytes, BytesMut};
use regex::bytes::{Captures, Regex, RegexBuilder};
use static_init::dynamic;
use time::Time;
use tokio_util::codec;

pub use message::*;

#[derive(Debug, Default)]
pub struct Decoder {
    state: State,
}

type Header = (Time, ThreadInfo, Level);

#[derive(Debug, Default)]
enum State {
    #[default]
    Raw,
    WithHeader(Header, Vec<Bytes>),
}

#[dynamic(try_init_once)]
static LOG_HEADER: Regex = RegexBuilder::new(
        r#"^ \[ (?<hour> \d{2} ) : (?<minute> \d{2} ) : (?<second> \d{2} ) \] \s \[ (?<thread>.+?) / (?<level>INFO|ERROR|WARN) \] : \s*"#
    )
    .unicode(true)
    .ignore_whitespace(true)
    .build()
    .unwrap();

impl Decoder {
    fn decode_line(&mut self, line: &mut Bytes) -> Result<Option<Message>> {
        let header = Self::extract_header(line)?;
        Ok(match header {
            Some(header) => {
                let message = self.flush();
                self.state = State::WithHeader(header, vec![line.clone()]);
                message
            }
            None => match &mut self.state {
                State::Raw => Some(Message::Raw(String::from_utf8_lossy(line).to_string())),
                State::WithHeader(_, ref mut lines) => {
                    lines.push(line.clone());
                    None
                }
            },
        })
    }

    fn extract_header(line: &mut Bytes) -> Result<Option<Header>> {
        fn extract<'a>(c: &'a Captures<'a>, name: &str) -> Cow<'a, str> {
            String::from_utf8_lossy(&c[name])
        }
        Ok(
            if let Some((end, header)) = LOG_HEADER
                .captures(&line[..])
                .map(|capture| {
                    Ok::<_, anyhow::Error>((
                        capture.get(0).unwrap().end(),
                        (
                            Time::from_hms(
                                extract(&capture, "hour").parse::<u8>()?,
                                extract(&capture, "minute").parse::<u8>()?,
                                extract(&capture, "second").parse::<u8>()?,
                            )?,
                            extract(&capture, "thread").into(),
                            extract(&capture, "level").parse()?,
                        ),
                    ))
                })
                .transpose()?
            {
                line.advance(end);
                Some(header)
            } else {
                None
            },
        )
    }

    fn flush(&mut self) -> Option<Message> {
        Self::build_message(std::mem::take(&mut self.state))
    }

    fn build_message(state: State) -> Option<Message> {
        match state {
            State::WithHeader((timestamp, thread, Level::Error), lines) => {
                let (stacktrace, message_lines) = lines
                    .into_iter()
                    .map(|line| String::from_utf8_lossy(&line).trim().to_string())
                    .partition::<Vec<_>, _>(|line| line.trim().starts_with("at "));
                Some(Message::Exception {
                    timestamp,
                    thread,
                    message: message_lines.join(" "),
                    stacktrace,
                })
            }
            State::WithHeader((timestamp, thread, level), lines) => {
                let message_lines = lines
                    .into_iter()
                    .map(|line| String::from_utf8_lossy(&line).trim().to_string())
                    .collect::<Vec<_>>();
                Some(Message::Formatted {
                    timestamp,
                    level,
                    thread,
                    message: message_lines.join(" "),
                })
            }
            State::Raw => None,
        }
    }
}

impl codec::Decoder for Decoder {
    type Item = Message;

    type Error = anyhow::Error;

    fn decode(&mut self, src: &mut BytesMut) -> Result<Option<Self::Item>> {
        while let Some(eol) = src.iter().position(|b| *b == b'\n') {
            let mut line = src.split_to(eol).freeze();
            src.advance(1);
            let msg = self.decode_line(&mut line)?;
            if msg.is_some() {
                return Ok(msg);
            }
        }
        if src.is_empty() {
            return Ok(self.flush());
        }
        Ok(None)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use codec::Decoder as _;

    #[test]
    fn decode_raw() {
        let mut input = BytesMut::from("Starting net.minecraft.server.Main\n");
        let mut decoder = Decoder::default();

        let output = decoder
            .decode(&mut input)
            .expect("should not fail")
            .expect("should yield a message");

        let Message::Raw(message) = output else {
            panic!("unexpected message: {output:?}");
        };
        assert_eq!(message, "Starting net.minecraft.server.Main");
    }

    #[test]
    fn decode_formatted_without_thread_number() {
        let mut input = BytesMut::from("[10:23:02] [Server thread/INFO]: Loading properties\n");
        let mut decoder = Decoder::default();

        let output = decoder
            .decode(&mut input)
            .expect("should not fail")
            .expect("should yield a message");

        let Message::Formatted {
            timestamp,
            level,
            thread,
            message,
        } = output
        else {
            panic!("unexpected message: {output:?}");
        };
        assert_eq!(timestamp, Time::from_hms(10, 23, 2).unwrap());
        assert_eq!(level, Level::Info);
        assert_eq!(thread.name(), "Server thread");
        assert_eq!(thread.index(), None);
        assert_eq!(message, "Loading properties");
    }

    #[test]
    fn decode_formatted_with_thread_number() {
        let mut input =
            BytesMut::from("[10:23:09] [Worker-Main-10/INFO]: Preparing spawn area: 0%\n");
        let mut decoder = Decoder::default();

        let output = decoder
            .decode(&mut input)
            .expect("should not fail")
            .expect("should yield a message");

        let Message::Formatted {
            timestamp,
            level,
            thread,
            message,
        } = output
        else {
            panic!("unexpected message: {output:?}");
        };
        assert_eq!(timestamp, Time::from_hms(10, 23, 9).unwrap());
        assert_eq!(level, Level::Info);
        assert_eq!(thread.name(), "Worker-Main");
        assert_eq!(thread.index(), Some(10));
        assert_eq!(message, "Preparing spawn area: 0%");
    }

    #[test]
    fn decode_exception() {
        let mut input = BytesMut::from(
            r#"[20:30:40] [ServerMain/ERROR]: Failed to start the minecraft server
atu$a: /home/user/minecraft/./world/session.lock: already locked (possibly by other Minecraft instance?)
    at atu$a.a(SourceFile:95) ~[server-1.20.4.jar:?]
    at atu.a(SourceFile:41) ~[server-1.20.4.jar:?]
    at egm$c.<init>(SourceFile:394) ~[server-1.20.4.jar:?]
    at egm.d(SourceFile:373) ~[server-1.20.4.jar:?]
    at net.minecraft.server.Main.main(SourceFile:133) ~[server-1.20.4.jar:?]
    at net.minecraft.bundler.Main.lambda$run$0(Main.java:54) ~[?:?]
    at java.lang.Thread.run(Thread.java:1589) ~[?:?]
"#,
        );
        let mut decoder = Decoder::default();

        let output = decoder
            .decode(&mut input)
            .expect("should not fail")
            .expect("should yield a message");

        let Message::Exception {
            timestamp,
            thread,
            message,
            stacktrace,
        } = output
        else {
            panic!("unexpected message: {output:?}");
        };
        assert_eq!(timestamp, Time::from_hms(20, 30, 40).unwrap());
        assert_eq!(thread.name(), "ServerMain");
        assert_eq!(message, "Failed to start the minecraft server atu$a: /home/user/minecraft/./world/session.lock: already locked (possibly by other Minecraft instance?)");
        assert_eq!(
            stacktrace,
            vec![
                "at atu$a.a(SourceFile:95) ~[server-1.20.4.jar:?]",
                "at atu.a(SourceFile:41) ~[server-1.20.4.jar:?]",
                "at egm$c.<init>(SourceFile:394) ~[server-1.20.4.jar:?]",
                "at egm.d(SourceFile:373) ~[server-1.20.4.jar:?]",
                "at net.minecraft.server.Main.main(SourceFile:133) ~[server-1.20.4.jar:?]",
                "at net.minecraft.bundler.Main.lambda$run$0(Main.java:54) ~[?:?]",
                "at java.lang.Thread.run(Thread.java:1589) ~[?:?]",
            ]
        );
    }
}
