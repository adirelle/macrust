use std::str::FromStr;
use std::sync::Arc;

use anyhow::bail;
use static_init::dynamic;
use time::Time;

// ======================================== Message ========================================

#[derive(Debug, Clone)]
pub enum Message {
    Raw(String),
    Formatted {
        timestamp: Time,
        level: Level,
        thread: ThreadInfo,
        message: String,
    },
    Exception {
        timestamp: Time,
        thread: ThreadInfo,
        message: String,
        stacktrace: Vec<String>,
    },
}

impl Message {
    #[cfg(test)]
    pub fn formatted<T: Into<ThreadInfo>, M: Into<String>>(thread: T, message: M) -> Self {
        Self::Formatted {
            timestamp: Time::from_hms(11, 10, 9).unwrap(),
            level: Level::Info,
            thread: thread.into(),
            message: message.into(),
        }
    }

    pub fn level(&self) -> Level {
        use Message::*;
        match self {
            Raw(_) => Level::Info,
            Exception { .. } => Level::Error,
            Formatted { level, .. } => *level,
        }
    }

    pub fn message(&self) -> &str {
        use Message::*;
        match self {
            Raw(message) | Formatted { message, .. } | Exception { message, .. } => {
                message.as_ref()
            }
        }
    }
}

impl std::fmt::Display for Message {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        use Message::*;
        match self {
            Raw(message) => message.fmt(f),
            Formatted {
                timestamp,
                level,
                thread,
                message,
            } => {
                let (hour, minute, second) = timestamp.as_hms();
                write!(
                    f,
                    "[time={hour:02}:{minute:02}:{second:02} thread={thread} level={level} message={message}]"
                )
            }
            Exception {
                timestamp,
                thread,
                message,
                ..
            } => {
                let (hour, minute, second) = timestamp.as_hms();
                write!(
                    f,
                    "[time={hour:02}:{minute:02}:{second:02} thread={thread} level=ERROR message={message}]"
                )
            }
        }
    }
}

// ======================================== ThreadName ========================================

#[derive(Debug, Eq, PartialEq, Clone)]
pub struct ThreadName(Arc<String>);

#[dynamic]
static THREADNAME_SERVER: ThreadName = ThreadName(Arc::new(String::from("Server thread")));

#[dynamic]
static THREADNAME_WORKER: ThreadName = ThreadName(Arc::new(String::from("Worker-Main")));

impl ThreadName {
    pub fn server() -> &'static ThreadName {
        &THREADNAME_SERVER
    }

    pub fn worker() -> &'static ThreadName {
        &THREADNAME_WORKER
    }
}

impl std::fmt::Display for ThreadName {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.0.fmt(f)
    }
}

impl<T: AsRef<str> + ToString> From<T> for ThreadName {
    fn from(value: T) -> Self {
        let str = value.as_ref();
        if Self::server() == str {
            Self::server().clone()
        } else if Self::worker() == str {
            Self::worker().clone()
        } else {
            Self(Arc::new(value.to_string()))
        }
    }
}

impl PartialEq<str> for ThreadName {
    fn eq(&self, other: &str) -> bool {
        self.0.as_ref().eq(other)
    }
}

impl PartialEq<&str> for ThreadName {
    fn eq(&self, other: &&str) -> bool {
        self.0.as_ref().eq(other)
    }
}

// ======================================== ThreadInfo ========================================

#[derive(Debug, Eq, PartialEq, Clone)]
pub enum ThreadInfo {
    Unique(ThreadName),
    Instance(ThreadName, u16),
}

impl ThreadInfo {
    pub fn unique<N: Into<ThreadName>>(name: N) -> Self {
        Self::Unique(name.into())
    }

    pub fn instance<N: Into<ThreadName>>(name: N, index: u16) -> Self {
        Self::Instance(name.into(), index)
    }

    pub fn name(&self) -> &ThreadName {
        match self {
            Self::Unique(name) | Self::Instance(name, _) => name,
        }
    }

    pub fn index(&self) -> Option<u16> {
        match self {
            Self::Instance(_, index) => Some(*index),
            _ => None,
        }
    }

    pub fn is_name<T>(&self, name: &T) -> bool
    where
        ThreadName: PartialEq<T>,
    {
        self.name().eq(name)
    }
}

impl std::fmt::Display for ThreadInfo {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Unique(name) => name.fmt(f),
            Self::Instance(name, number) => write!(f, "{name}#{number}"),
        }
    }
}

impl<T: AsRef<str>> From<T> for ThreadInfo {
    fn from(value: T) -> Self {
        let value = value.as_ref();
        if let Some((head, tail)) = value.rsplit_once('-') {
            if let Ok(index) = tail.parse::<u16>() {
                return Self::instance(head, index);
            }
        }
        Self::unique(value)
    }
}

impl AsRef<ThreadName> for ThreadInfo {
    fn as_ref(&self) -> &ThreadName {
        self.name()
    }
}

// ======================================== Level ========================================

#[derive(Debug, Eq, PartialEq, Ord, PartialOrd, Clone, Copy)]
pub enum Level {
    Info,
    Warn,
    Error,
}

impl FromStr for Level {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> std::prelude::v1::Result<Self, Self::Err> {
        Ok(match s {
            "INFO" => Self::Info,
            "WARN" => Self::Warn,
            "ERROR" => Self::Error,
            _ => bail!("unknown level: {s}"),
        })
    }
}

impl std::fmt::Display for Level {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        use Level::*;
        f.write_str(match self {
            Info => "INFO",
            Warn => "WARN",
            Error => "ERROR",
        })
    }
}

impl From<Level> for tracing::Level {
    fn from(value: Level) -> Self {
        use Level::*;
        match value {
            Info => Self::INFO,
            Warn => Self::WARN,
            Error => Self::WARN,
        }
    }
}

// ======================================== tests ========================================
#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn parse_threadinfo_unique_1() {
        let info = ThreadInfo::from("foobar");

        assert_eq!(info, ThreadInfo::unique("foobar"));
        assert!(info.is_name(&"foobar"));
        assert_eq!(info.name(), "foobar");
        assert_eq!(info.index(), None);
    }

    #[test]
    fn parse_threadinfo_unique_2() {
        let info = ThreadInfo::from("foo-bar");

        assert_eq!(info, ThreadInfo::unique("foo-bar"));
        assert!(info.is_name(&"foo-bar"));
        assert_eq!(info.name(), "foo-bar");
        assert_eq!(info.index(), None);
    }

    #[test]
    fn parse_threadinfo_instance() {
        let info = ThreadInfo::from("foobar-5");

        assert_eq!(info, ThreadInfo::instance("foobar", 5));
        assert!(info.is_name(&"foobar"));
        assert_eq!(info.name(), "foobar");
        assert_eq!(info.index(), Some(5));
    }
}
