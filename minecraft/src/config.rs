use std::env;
use std::path::{Path, PathBuf};

use serde::Deserialize;
use tokio::process::Command;
use which::which;

// ======================================== ConfigBuilder ========================================

#[derive(Debug, Deserialize)]
#[serde(default)]
pub struct ConfigBuilder {
    base_dir: PathBuf,
    working_dir: PathBuf,
    java: PathBuf,
    java_args: Vec<String>,
    server_jar: PathBuf,
    server_args: Vec<String>,
}

impl Default for ConfigBuilder {
    fn default() -> Self {
        Self {
            base_dir: ".".into(),
            working_dir: ".".into(),
            java: "java".into(),
            java_args: Vec::new(),
            server_jar: "server.jar".into(),
            server_args: Vec::new(),
        }
    }
}

impl ConfigBuilder {
    pub fn working_dir(mut self, working_dir: PathBuf) -> Self {
        self.working_dir = working_dir;
        self
    }

    pub fn java(mut self, java: PathBuf) -> Self {
        self.java = java;
        self
    }

    pub fn server_jar(mut self, server_jar: PathBuf) -> Self {
        self.server_jar = server_jar;
        self
    }

    pub fn build(self) -> Result<Config, Error> {
        let Self {
            base_dir,
            working_dir,
            java,
            java_args,
            server_jar,
            server_args,
        } = self;
        let base_dir = find_path([env::current_dir()?], base_dir, is_directory_check)?;
        let working_dir = find_path([&base_dir], working_dir, is_directory_check)?;
        let search = [&base_dir, &working_dir];
        let java = which(java)?;
        let server_jar = find_path(search, server_jar, is_file_check)?;
        Ok(Config {
            working_dir,
            java,
            java_args,
            server_jar,
            server_args,
        })
    }
}

fn find_path<F, I, T, P>(paths: I, name: P, check: F) -> Result<PathBuf, Error>
where
    P: AsRef<Path>,
    F: Fn(&Path) -> Result<&Path, Error>,
    I: IntoIterator<Item = T>,
    T: AsRef<Path>,
{
    let name = name.as_ref();
    if name.is_absolute() {
        check(name)?;
        return Ok(name.into());
    }
    let mut last_result = Err(Error::NotFound(name.to_owned()));
    for path in paths {
        let path = path.as_ref();
        assert!(path.is_absolute());
        let Ok(full_path) = path.join(name).canonicalize() else {
            continue;
        };
        last_result = check(&full_path).map(Path::to_path_buf);
        if last_result.is_ok() {
            break;
        }
    }
    last_result
}

// ======================================== Error ========================================

#[derive(Debug, thiserror::Error)]
pub enum Error {
    #[error("file or directory not found: {0}")]
    NotFound(PathBuf),

    #[error("must be a file: {0}")]
    ExpectedFile(PathBuf),

    #[error("must be a directory: {0}")]
    ExpectedDirectory(PathBuf),

    #[error("command not found: {0}")]
    Which(#[from] which::Error),

    #[error(transparent)]
    IO(#[from] std::io::Error),
}

// ======================================== Config ========================================

#[derive(Debug, Clone)]
pub struct Config {
    working_dir: PathBuf,
    java: PathBuf,
    java_args: Vec<String>,
    server_jar: PathBuf,
    server_args: Vec<String>,
}

impl Config {
    pub fn command(&self) -> Command {
        let Self {
            java,
            java_args,
            server_jar,
            server_args,
            working_dir,
        } = self.clone();
        let mut cmd = Command::new(java);
        cmd.args(java_args)
            .arg("-jar")
            .arg(server_jar)
            .arg("nogui")
            .args(server_args)
            .current_dir(working_dir);
        cmd
    }
}

// ======================================== checks ========================================

fn is_file_check(path: &Path) -> Result<&Path, Error> {
    if path.is_file() {
        Ok(path)
    } else if !path.exists() {
        Err(Error::NotFound(path.to_owned()))
    } else {
        Err(Error::ExpectedFile(path.to_owned()))
    }
}

fn is_directory_check(path: &Path) -> Result<&Path, Error> {
    if path.is_dir() {
        Ok(path)
    } else if !path.exists() {
        Err(Error::NotFound(path.to_owned()))
    } else {
        Err(Error::ExpectedDirectory(path.to_owned()))
    }
}
