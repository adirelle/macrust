// ======================================== Encoder ========================================

use anyhow::Result;
use bytes::BufMut;
use tokio_util::codec;

#[derive(Debug, Default)]
pub struct Encoder;

impl codec::Encoder<Command> for Encoder {
    type Error = anyhow::Error;

    fn encode(&mut self, cmd: Command, dst: &mut bytes::BytesMut) -> Result<()> {
        use Command::*;
        let bytes = match cmd {
            Stop => b"/stop\n",
        };
        tracing::debug!(cmd=%bytes.escape_ascii(), "writing to stdin");
        dst.put_slice(bytes);
        Ok(())
    }
}

// ======================================== Command ========================================

#[derive(Debug)]
pub enum Command {
    Stop,
}
