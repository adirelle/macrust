use std::sync::Arc;
use std::time::Duration;

use anyhow::Result;
use tokio::sync::{broadcast, mpsc, watch};
use tokio::time::sleep;

use super::config::Config;
use super::event::Event;
use super::input::Command;
use super::Server;

#[derive(Debug)]
pub struct Controller {
    events_tx: broadcast::Sender<Event>,
    target_tx: watch::Sender<Target>,
    state_rx: watch::Receiver<State>,
}

impl Controller {
    pub fn new(config: Arc<Config>) -> Self {
        let (events_tx, events_rx) = broadcast::channel(50);
        let (target_tx, target_rx) = watch::channel(Target::Started);
        let (state_tx, state_rx) = watch::channel(State::Stopped);
        {
            let events_tx = events_tx.clone();
            tokio::spawn(async move {
                let runner = Runner::new(config, events_tx, events_rx, target_rx, state_tx);
                runner.run().await;
            });
        }
        Self {
            events_tx,
            target_tx,
            state_rx,
        }
    }

    pub fn events(&self) -> broadcast::Receiver<Event> {
        self.events_tx.subscribe()
    }

    pub fn start(&mut self) -> Result<()> {
        self.target_tx.send(Target::Started).map_err(Into::into)
    }

    pub fn stop(&mut self) -> Result<()> {
        self.target_tx.send(Target::Stopped).map_err(Into::into)
    }

    pub async fn started(&mut self) -> Result<()> {
        self.state_rx.wait_for(State::is_started).await?;
        Ok(())
    }

    pub async fn stopped(&mut self) -> Result<()> {
        self.state_rx.wait_for(State::is_stopped).await?;
        Ok(())
    }

    pub async fn restart(&mut self) -> Result<()> {
        self.stop()?;
        self.stopped().await?;
        self.start()?;
        self.started().await
    }

    pub async fn terminate(mut self) -> Result<()> {
        self.stop()?;
        self.stopped().await
    }
}

#[derive(Debug)]
struct Runner {
    config: Arc<Config>,
    events_tx: broadcast::Sender<Event>,
    events_rx: broadcast::Receiver<Event>,
    target_rx: watch::Receiver<Target>,
    state_tx: watch::Sender<State>,
}

impl Runner {
    fn new(
        config: Arc<Config>,
        events_tx: broadcast::Sender<Event>,
        events_rx: broadcast::Receiver<Event>,
        target_rx: watch::Receiver<Target>,
        state_tx: watch::Sender<State>,
    ) -> Self {
        Self {
            config,
            events_tx,
            events_rx,
            target_rx,
            state_tx,
        }
    }

    #[tracing::instrument(name = "controller", skip_all)]
    async fn run(mut self) {
        while self.target_rx.borrow_and_update().is_started() {
            let (result, _) = tokio::join!(self.run_once(), sleep(Duration::from_secs(1)));
            tracing::info!(?result, "ran once");
        }
    }

    async fn run_once(&mut self) -> Result<()> {
        let (cmds_tx, cmds_rx) = mpsc::channel(5);
        let server = Server::run(self.config.clone(), cmds_rx, self.events_tx.clone());

        let result = tokio::select! {
            _ = server => {
                tracing::debug!("server stopped");
                Ok(())
            },
            result = self.control(cmds_tx) => {
                tracing::debug!("control returned");
                result
            }
        };

        self.state_tx.send(State::Stopped)?;

        result
    }

    #[tracing::instrument(skip_all)]
    async fn control(&mut self, cmds_tx: mpsc::Sender<Command>) -> Result<()> {
        self.state_tx.send(State::Starting)?;

        Self::wait_for_event(&mut self.events_rx, Event::is_server_ready).await;

        self.state_tx.send(State::Started)?;

        let send_stop_cmd = tokio::select! {
            _ = self.target_rx.wait_for(Target::is_stopped) => true,
            _ = Self::wait_for_event(&mut self.events_rx, Event::is_server_stopping) => false
        };
        if send_stop_cmd {
            cmds_tx.send(Command::Stop).await?;
        }

        self.state_tx.send(State::Stopping)?;

        Self::wait_for_event(&mut self.events_rx, Event::is_server_stopped).await;

        Ok(())
    }

    async fn wait_for_event<F>(
        events: &mut broadcast::Receiver<Event>,
        predicate: F,
    ) -> Option<Event>
    where
        F: Fn(&Event) -> bool,
    {
        while let Ok(event) = events.recv().await {
            if predicate(&event) {
                return Some(event);
            }
        }
        None
    }
}

#[derive(Debug, Clone, Copy)]
enum Target {
    Started,
    Stopped,
}

impl Target {
    #[inline]
    pub fn is_started(&self) -> bool {
        matches!(self, Self::Started)
    }

    #[inline]
    pub fn is_stopped(&self) -> bool {
        matches!(self, Self::Stopped)
    }
}

#[derive(Debug, Clone, Copy)]
enum State {
    Starting,
    Started,
    Stopping,
    Stopped,
}

impl State {
    fn is_started(&self) -> bool {
        matches!(self, Self::Started)
    }

    fn is_stopped(&self) -> bool {
        matches!(self, Self::Stopped)
    }
}
