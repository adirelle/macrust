use std::net::SocketAddr;
use std::os::unix::process::ExitStatusExt;
use std::process::ExitStatus;

use anyhow::Result;
use regex::{Regex, RegexBuilder};
use static_init::dynamic;

use super::output::{Level, Message, ThreadName};

#[dynamic(try_init_once)]
static EVENT_REGEX: Regex = RegexBuilder::new(
    r#"^ \s* (?:
        (?<READY>
            # Done (8.992s)! For help, type "help"
            Done \s \(\d+\.\d+s\)! \s For \s help, \s type \s "help"

        ) | (?:
            # Starting minecraft server version 1.20.4
            Starting \s minecraft \s server \s version \s (?<SERVER_VERSION> \d+ (?: \.\d+ )+ )

        ) | (?<STOPPING_SERVER>
            # Stopping server
            Stopping \s server

        ) | (?:
            # Adirelle[/127.0.0.1:56356] logged in with entity id 259 at (-7.890902949337502, 84.0, 20.129866144646666)
            (?<PLAYER_LOGGED_IN> \w+ ) \[ (?: / (?<SOCKET> \d+\.\d+\.\d+\.\d+ : \d+ ) | IP \s hidden ) \] \s
            logged \s in \s with \s entity \s id \s \d+ \s at \s
            \( -?\d+\.\d+, \s -?\d+\.\d+, \s -?\d+\.\d+ \)

        ) | (?:
            # Adirelle lost connection: Disconnected
            # Adirelle (IP hidden) lost connection: Disconnected
            # Adirelle (/127.0.0.1:35000) lost connection: Disconnected
            (?<PLAYER_DISCONNECTED> \w+ ) \s (?: \( (?: IP \s hidden | / \d+\.\d+\.\d+\.\d+ : \d+ ) \) \s )? lost \s connection: \s (?<REASON> .*)

        ) | (?:
            # <Adirelle> foo bar
            < (?<PLAYER_SAYS> \w+ ) > \s (?<SAY> .*)

        ) | (?:
            # * Adirelle foo bar
            \* \s (?<PLAYER_EMOTES> \w+ ) \s (?<EMOTE> .*)

        ) | (?:
            # [Adirelle: Set the time to 6000]
            \[ (?<PLAYER_COMMAND> \w+ ) : \s (?<RESULT> .*?) \]

        )
    ) \s* $"#,
)
.unicode(true)
.ignore_whitespace(true)
.build()
.unwrap();

// ======================================== Event ========================================

#[derive(Debug, Clone)]
pub enum Event {
    Server { event: ServerEvent },
    Player { name: String, event: PlayerEvent },
    Message { message: Message },
}

impl Event {
    pub fn player<N: ToString>(name: N, event: PlayerEvent) -> Self {
        Self::Player {
            name: name.to_string(),
            event,
        }
    }

    pub fn server<E: Into<ServerEvent>>(event: E) -> Self {
        Self::Server {
            event: event.into(),
        }
    }

    pub fn is_server_event(&self) -> bool {
        matches!(self, Self::Server { .. })
    }

    pub fn as_server_event(&self) -> Option<&ServerEvent> {
        match self {
            Self::Server { ref event } => Some(event),
            _ => None,
        }
    }

    pub fn is_server_starting(&self) -> bool {
        self.as_server_event().is_some_and(ServerEvent::is_starting)
    }

    pub fn is_server_ready(&self) -> bool {
        self.as_server_event().is_some_and(ServerEvent::is_ready)
    }

    pub fn is_server_stopping(&self) -> bool {
        self.as_server_event().is_some_and(ServerEvent::is_stopping)
    }

    pub fn is_server_stopped(&self) -> bool {
        self.as_server_event().is_some_and(ServerEvent::is_stopped)
    }

    pub fn is_player_event(&self) -> bool {
        matches!(self, Self::Player { .. })
    }

    pub fn as_player_event(&self) -> Option<(&str, &PlayerEvent)> {
        match self {
            Self::Player {
                ref name,
                ref event,
            } => Some((&name, event)),
            _ => None,
        }
    }

    fn parse(message: &str) -> Option<Self> {
        EVENT_REGEX.captures(message).map(|capture| {
            if capture.name("READY").is_some() {
                ServerEvent::Ready.into()
            } else if let Some(version) = capture.name("SERVER_VERSION") {
                ServerEvent::Version {
                    version: version.as_str().to_string(),
                }
                .into()
            } else if capture.name("STOPPING_SERVER").is_some() {
                ServerEvent::Stopping.into()
            } else if let Some(name) = capture.name("PLAYER_LOGGED_IN") {
                let addr = capture.name("SOCKET").map(|socket| {
                    socket
                        .as_str()
                        .parse()
                        .expect("addr already checked by regex should not fail to parse")
                });
                Self::player(name.as_str(), PlayerEvent::LoggedIn { addr })
            } else if let Some(name) = capture.name("PLAYER_DISCONNECTED") {
                Self::player(
                    name.as_str(),
                    PlayerEvent::Disconnected {
                        reason: capture["REASON"].to_string(),
                    },
                )
            } else if let Some(name) = capture.name("PLAYER_SAYS") {
                Self::player(
                    name.as_str(),
                    PlayerEvent::Says {
                        message: capture["SAY"].to_string(),
                    },
                )
            } else if let Some(name) = capture.name("PLAYER_EMOTES") {
                Self::player(
                    name.as_str(),
                    PlayerEvent::Emote {
                        emote: capture["EMOTE"].to_string(),
                    },
                )
            } else if let Some(name) = capture.name("PLAYER_COMMAND") {
                Self::player(
                    name.as_str(),
                    PlayerEvent::Command {
                        result: capture["RESULT"].to_string(),
                    },
                )
            } else {
                unreachable!(
                    "missing test for {:?}",
                    capture
                        .iter()
                        .zip(EVENT_REGEX.capture_names())
                        .filter_map(|pair| match pair {
                            (Some(value), Some(name)) => Some((name, value.as_str())),
                            _ => None,
                        })
                        .collect::<Vec<_>>()
                )
            }
        })
    }
}

impl TryFrom<Message> for Event {
    type Error = anyhow::Error;

    fn try_from(value: Message) -> Result<Self> {
        Ok(match value {
            Message::Formatted {
                ref thread,
                ref message,
                level: Level::Info,
                ..
            } if thread.is_name(ThreadName::server()) => {
                Self::parse(message).unwrap_or(Self::Message { message: value })
            }
            _ => Self::Message { message: value },
        })
    }
}

impl From<ServerEvent> for Event {
    fn from(event: ServerEvent) -> Self {
        Self::Server { event }
    }
}

impl<E: ToString> From<Result<ExitStatus, E>> for Event {
    fn from(value: Result<ExitStatus, E>) -> Self {
        Self::Server {
            event: value.into(),
        }
    }
}

// ======================================== PlayerEvent ========================================

#[derive(Debug, Clone)]
pub enum PlayerEvent {
    LoggedIn { addr: Option<SocketAddr> },
    Disconnected { reason: String },
    Says { message: String },
    Emote { emote: String },
    Command { result: String },
}

// ======================================== ServerEvent ========================================

#[derive(Debug, Clone)]
pub enum ServerEvent {
    Starting { pid: u32 },
    Version { version: String },
    Ready,
    Stopping,
    Exited { code: i32 },
    Killed { signal: i32 },
    Failed { error: String },
}

impl ServerEvent {
    pub fn is_starting(&self) -> bool {
        matches!(self, Self::Starting { .. })
    }

    pub fn is_ready(&self) -> bool {
        matches!(self, Self::Ready)
    }

    pub fn is_stopping(&self) -> bool {
        matches!(self, Self::Stopping)
    }

    pub fn is_stopped(&self) -> bool {
        matches!(
            self,
            Self::Exited { .. } | Self::Killed { .. } | Self::Failed { .. }
        )
    }
}

impl<E: ToString> From<Result<ExitStatus, E>> for ServerEvent {
    fn from(value: Result<ExitStatus, E>) -> Self {
        match value {
            Err(error) => Self::Failed {
                error: error.to_string(),
            },
            Ok(status) => status
                .signal()
                .map(|signal| Self::Killed { signal })
                .unwrap_or_else(|| Self::Exited {
                    code: status.code().unwrap(),
                }),
        }
    }
}

// ======================================== tests ========================================

#[cfg(test)]
mod tests {
    use super::*;

    macro_rules! test_parse {
        { $test_name:ident: $pattern:pat = $msg:literal } => {
            test_parse!{ $test_name: $pattern = $msg => {} }
        };
        { $test_name:ident: $pattern:pat = $msg:literal => $asserts:block } => {
            #[test]
            fn $test_name() {
                let msg = Message::formatted("Server thread", $msg);
                let event = msg.try_into().unwrap();
                let $pattern = event else {
                    panic!("{event:?} did not match {}", stringify!($pattern));
                };
                $asserts
            }
        };
    }

    test_parse! { parse_ready_event:
        Event::Server { event: ServerEvent::Ready } = r#"Done (8.992s)! For help, type "help""#
    }

    test_parse! { parse_server_version:
        Event::Server { event: ServerEvent::Version { version } } = r#"Starting minecraft server version 1.20.4"#
        => {
            assert_eq!(version, "1.20.4");
        }
    }

    test_parse! { parse_played_logged_in:
        Event::Player{ name, event: PlayerEvent::LoggedIn { addr: Some(addr) } } = r#"Adirelle[/127.0.0.1:56356] logged in with entity id 259 at (-7.890902949337502, 84.0, 20.129866144646666)"#
        => {
            assert_eq!(name, "Adirelle");
            assert_eq!(addr, "127.0.0.1:56356".parse().unwrap());
        }
    }

    test_parse! { parse_played_logged_in_no_ip:
        Event::Player{ name, event: PlayerEvent::LoggedIn { addr: None } }  = r#"Adirelle[IP hidden] logged in with entity id 259 at (-7.890902949337502, 84.0, 20.129866144646666)"#
        => {
            assert_eq!(name, "Adirelle");
        }
    }

    test_parse! { parse_disconnected:
        Event::Player { name, event: PlayerEvent::Disconnected { reason } } = r#"Adirelle lost connection: Disconnected"#
        => {
            assert_eq!(name, "Adirelle");
            assert_eq!(reason, "Disconnected");
        }
    }

    test_parse! { parse_disconnected_hidden_ip:
        Event::Player { name, event: PlayerEvent::Disconnected { reason } } = r#"Adirelle (IP hidden) lost connection: Disconnected"#
        => {
            assert_eq!(name, "Adirelle");
            assert_eq!(reason, "Disconnected");
        }
    }

    test_parse! { parse_disconnected_with_ip:
        Event::Player { name, event: PlayerEvent::Disconnected { reason } } = r#"Adirelle (/127.0.0.1:54354) lost connection: Disconnected"#
        => {
            assert_eq!(name, "Adirelle");
            assert_eq!(reason, "Disconnected");
        }
    }

    test_parse! { parse_says:
        Event::Player { name, event: PlayerEvent::Says { message } } = r#"<Adirelle> foo bar"#
        => {
            assert_eq!(name, "Adirelle");
            assert_eq!(message, "foo bar");
        }
    }

    test_parse! { parse_emote:
        Event::Player { name, event: PlayerEvent::Emote { emote } }  = r#"* Adirelle foo bar"#
        => {
            assert_eq!(name, "Adirelle");
            assert_eq!(emote, "foo bar");
        }
    }

    test_parse! { parse_command:
        Event::Player { name, event: PlayerEvent::Command { result } }  = r#"[Adirelle: foo bar]"#
        => {
            assert_eq!(name, "Adirelle");
            assert_eq!(result, "foo bar");
        }
    }
}
